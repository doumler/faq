<?php

namespace controllers;
use models\Admin;

class AdminController extends CoreController{


    function __construct()
	{
		
		$this->model = new Admin();
		
	}
	
	
    public function getLogin()
	{
	
	if($_SESSION['users'])
	{
		header('Location: ?/admin/list');
	}
	
	echo $this->render('login.php');
	
	}
	
	/**
	*	Authorization of an admin
	*/
    public function postLogin($params, $post)
	{
		
	$log = '';
	$pass = '';
	$message = [];
			
	if(isset($post['auth']))
	{
	
	$log = $post['login'];
	$pass = $post['password'];

	if($log =='')
	{
		$message[] = "Insert your login";
	}
	if($pass === '') 
	{
		$message[] = "Insert your password";
	}
	if(!$this->model->findAuth($log, $pass)){
		
		$message[] = "Incorrect login or password";
	}
	else{
		$is_auth = $this->model->findAuth($log, $pass);
		
		$_SESSION['users'] = $is_auth;
	
		header('Location: ?/admin/list');
		
	}
    }
	echo $this->render('login.php', ['message' => $message, 'log' => $log]);
    }


	/**
	*  page with navigation in admin panel
	*/
    public function getList()
	{
	
	$session = $this->model->getCurrentUser();
	
	echo $this->render('admin.php',['session' =>$session]);
	
    }

	/**
	*  show all admins
	*/
    public function getUser()
	{
	
	$session = $this->model->getCurrentUser();
	
	$adminList = $this->model->selectAllAdmin();
	echo $this->render('show_admin.php', ['adminList' => $adminList]);
    }

	/**
	*  add a new admin
	*/
    public function postAdd($params, $post)
	{
	$session = $this->model->getCurrentUser();
	
	if($post)
	{
		$name = (string)$post['name'];
		$password = (string)$post['password'];
		$this->model->addNewAdmin($name, $password);
		header('Location: ?/admin/user');
	}
    }
	
	/**
	*	Delete an admin
	*/
	public function getDelete($params)
	{
		$session = $this->model->getCurrentUser();
		if(!empty($session)){
		if (isset($params['id']) && is_numeric($params['id'])) {
			$this->model->deleteAdmin($params['id']);
		
				header('Location: ?/admin/user');
			}
		}
	}
	
	/**
	*	Form to edit 
	*/
	public function getUpdate($params)
	{
		$session = $this->model->getCurrentUser();
		if (isset($params['id']) && is_numeric($params['id'])) {
				
				$adminToEdit = $this->model->selectOneAdmin($params['id']);
				echo $this->render('update_admin.php',['adminToEdit'=> $adminToEdit]);
			}
		
	}
	
	/**
	*	Edition of password
	*/
	public function postUpdate($params, $post)
	{
		$session = $this->model->getCurrentUser();
		if (isset($params['id']) && is_numeric($params['id'])) {
		
			if (isset($post['password'])) {
				$update = $post['password'];
			}
			
			
			$this->model->updateAdminPass($params['id'], $update);
			header('Location: ?/admin/user');
			
		}
	}
	
	/**
	*	exit from admin panel
	*/
	public function getLogout()
	{
		session_start();
		session_destroy();
		header('Location: ?/admin/login/');
		
	}

}


