<?php
namespace controllers;
use models\Category;

class CategoryController extends CoreController{


    function __construct()
	{
		
		$this->model = new Category();
		
	}
	/**
	*	show all themes
	*/
    public function getList()
	{
	
	$session = $this->model->getCurrentUser();
	
	$getAllCategories = $this->model->showCategoriesList();
	
	echo $this->render('show_category.php', ['getAllCategories' => $getAllCategories]);
	
    }

	/**
	*	add a new theme
	*/
    public function postAdd($params, $post)
	{
	$session = $this->model->getCurrentUser();
	
	if($post){
		$name = (string)$post['name'];
		$this->model->createCategory($name);
		header('Location: ?/category/list');
	}
    }
	
	/**
	* deletion of a theme
	*/
	public function getDelete($params)
	{
		$session = $this->model->getCurrentUser();
		if(!empty($session)){
		if (isset($params['cat']) && is_numeric($params['cat'])) {

			$this->model->delCatAndRequest($params['cat']);
		
				header('Location: ?/category/list');
			}	
		}
	}
	
	

}



