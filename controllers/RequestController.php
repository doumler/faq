<?php
namespace controllers;
use models\Request;

class RequestController extends CoreController{


    function __construct()
	{
		
		$this->model = new Request();
		
	}
	
	/**
	*	show one theme with related questions
	*/
    public function getEntry($params)
	{
	
	$session = $this->model->getCurrentUser();
	
	if (isset($params['cat']) && is_numeric($params['cat'])) {
				
				$entry = $this->model->showOneCat($params['cat']);
		
				
				echo $this->render('show_entry.php',['entry'=> $entry]);
			}
	
    }

	/**
	*	add a new question
	*/
    public function postAdd($params, $post){
	$session = $this->model->getCurrentUser();
	$text= (string)$post['text'];
	$id = (int)$params['cat'];
	$this->model->addRequest($text, $id, $session['name']);
	header('Location: ?/request/entry/cat/'.$params['cat']);
    }
	
	/**
	*	delete a question
	*/
    public function getDelete($params)
	{
		$session = $this->model->getCurrentUser();
		if(!empty($session)){
		if (isset($params['id']) && is_numeric($params['id'])) {

			$this->model->delRequest($params['id']);
		
				header('Location: ?/request/entry/cat/'.$params['cat']);
			
		}
	  }
	}
	
	/**
	*	form to edit
	*/
    public function getEdit($params){
	
	$session = $this->model->getCurrentUser();
	
	if(isset($params)){
		
		$entryToEdit = $this->model->showOneRequest((int)$params['cat'], (int)$params['id']);
		
		
	}
	$categories = $this->model->findAllСategories();

	echo $this->render('edit_entry.php', ['entryToEdit'=>$entryToEdit,'categories'=>$categories]);
    }
	
	/**
	*	edit / add an answer to a related question
	*/
    public function postUpdate($params, $post)
	{
	
	$session = $this->model->getCurrentUser();
	
	if(isset($post)){
		
		$updateArray = $post;
		$id = (int)$params['id'];
		$hasResponse = $this->model->checkResponse($id);
		if($hasResponse){
			$this->model->entryUpdate($id, $updateArray);
		}
		else{
			$this->model->entryUpdateInsert($id, $updateArray);
		}
		
		header('Location: ?/request/entry/cat/'.$params['cat']);
	}
	
	
    }
	
	/**
	* show all new questions
	*/
    public function getNew()
	{
	
	$newEntries = $this->model->showNewRequest();


	echo $this->render('new_entry.php', ['newEntries'=> $newEntries]);
		
    }

}



