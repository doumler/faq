<?php

namespace controllers;
use models\Main;

class MainController extends CoreController
{
	
	function __construct()
	{
		
		$this->model = new Main();
		
	}
	

	/**
	 * Show all themes and related questions
	 * @return array
	 */
	public function getList()
	{
		
		$data = $this->model->findAll();
		
		echo $this->render('view.php', ['data' =>$data]);
		
	}
	
	public function getForm(){
		
		$categories = $this->model->findAllСategories();
		echo $this->render('form.php', ['categories' =>$categories]);
	}
	
	
	/**
	 * post a question by site visitor
	 * 
	 */
	public function postForm($params, $post){
	$name = '';
	$email = '';
	$text = '';
	$category = '';
	$message= [];
	$categories = $this->model->findAllСategories();
	if(isset($post))
	{
	$name = (string)$post['name'];
	$email = (string)$post['email'];
	$text = (string)$post['text'];
	$category = $post['cat'];
	

	if($name==='')
	{
		$message['error'][] = "Insert your name";
	}
	if($email==='') 
	{
		$message['error'][] = "Insert your email";
	}
	if($text ==='') 
	{
		$message['error'][] = "Add your question";
	}

	
	if(empty($message))
	{
	
	if($this->model->add(['text'=>$text, 'category' => $category, 'name' => $name, 'email' => $email]))
		{
		$message['success'][] = "Danke fuer Ihre Anfrage! Wir bearbeiten Ihre Anfrage sobald wie moeglich";
		}
	}
	}
	echo $this->render('form.php', ['message'=>$message, 'categories' =>$categories, 'name' =>$name, 'email'=>$email, 'text'=>$text]);	
	
	
}

	
}

