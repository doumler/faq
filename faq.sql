-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 22 2017 г., 14:45
-- Версия сервера: 5.5.48
-- Версия PHP: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `faq`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admin`
--

INSERT INTO `admin` (`id`, `name`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(6, 'Alex', '202cb962ac59075b964b07152d234b70'),
(7, 'Marina', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Структура таблицы `author`
--

CREATE TABLE IF NOT EXISTS `author` (
  `id` int(11) unsigned NOT NULL,
  `request_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `author`
--

INSERT INTO `author` (`id`, `request_id`, `name`, `email`) VALUES
(13, 83, 'admin', NULL),
(15, 84, 'admin', NULL),
(16, 85, 'admin', NULL),
(17, 86, 'admin', NULL),
(18, 87, 'incognito', NULL),
(19, 88, 'admin', NULL),
(20, 89, 'admin', NULL),
(21, 90, 'admin', NULL),
(22, 91, 'admin', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`category_id`, `title`) VALUES
(30, 'Geld online senden'),
(31, 'Geld an ein Bankkonto senden'),
(32, 'Geld persönlich senden'),
(33, 'Registrierung'),
(34, 'Schutz unserer Kunden');

-- --------------------------------------------------------

--
-- Структура таблицы `request`
--

CREATE TABLE IF NOT EXISTS `request` (
  `id` int(11) unsigned NOT NULL,
  `text` text NOT NULL,
  `cat_id` int(11) NOT NULL,
  `is_published` enum('0','1') NOT NULL DEFAULT '0',
  `dated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `request`
--

INSERT INTO `request` (`id`, `text`, `cat_id`, `is_published`, `dated`) VALUES
(83, 'Wie kann ich online Geld senden?', 30, '1', '2017-05-22 11:31:17'),
(84, 'Wie bezahle ich?', 30, '0', '2017-05-22 10:54:17'),
(85, 'Wie überweise ich Geld direkt auf ein Bankkonto?', 31, '1', '2017-05-22 11:43:01'),
(86, 'Was ist eine Bankleitzahl?', 31, '0', '2017-05-22 10:55:35'),
(87, 'Wie hole ich einen Geldtransfer ab?', 32, '1', '2017-05-22 11:43:08'),
(88, 'Wie viel Geld kann ich von einem Standort senden?', 32, '0', '2017-05-22 10:57:42'),
(89, 'Ist die Registrierung auf der Website von der Bank kostenpflichtig?', 33, '1', '2017-05-22 11:43:22'),
(90, 'Auf welche häufigen Kundenbetrugsszenarien sollte man achten?', 34, '1', '2017-05-22 11:43:31'),
(91, 'Was unternimmt die Bank, um meine Daten zu schützen?', 34, '0', '2017-05-22 11:14:59');

-- --------------------------------------------------------

--
-- Структура таблицы `response`
--

CREATE TABLE IF NOT EXISTS `response` (
  `id` int(11) unsigned NOT NULL,
  `request_id` int(11) NOT NULL,
  `text` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `response`
--

INSERT INTO `response` (`id`, `request_id`, `text`, `date`) VALUES
(45, 83, 'Sie können Geld online für eine Bargeldabholung an einen Western Union-Standort senden, direkt an das Bankkonto des Empfängers oder an das Mobiltelefon des Empfängers. Nachdem Sie das Bestimmungsland ausgewählt haben, sehen Sie, welche Servicearten verfügbar sind.', '2017-05-22 11:31:00'),
(46, 85, 'Dieser Service steht für bestimmte Länder zur Verfügung. Normalerweise wird das Geld innerhalb von 1 bis 2 Werktagen dem Konto des Empfängers gutgeschrieben. Das Geld kann am selben Tag oder innerhalb von bis zu 3 Werktagen gutgeschrieben werden, je nachdem, wann und wo es gesendet wurde.', '2017-05-22 11:33:28'),
(47, 87, 'Sie können das Geld an jedem Western Union-Standort abholen. Sie müssen das Formular "Geld empfangen" ausfüllen und folgende Informationen angeben:\r\n', '2017-05-22 11:37:52'),
(48, 89, 'Nein, die Registrierung ist kostenlos. Sie können sich hier registrieren. Es dauert nur 1 Minute.', '2017-05-22 11:40:35'),
(49, 90, 'Seien Sie vorsichtig bei Briefen oder E-Mails von Unbekannten, in denen teure oder seltene Waren zu unrealistischen Preisen angeboten werden.', '2017-05-22 11:42:38');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Индексы таблицы `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `response`
--
ALTER TABLE `response`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `author`
--
ALTER TABLE `author`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT для таблицы `request`
--
ALTER TABLE `request`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT для таблицы `response`
--
ALTER TABLE `response`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
