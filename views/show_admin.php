{% include 'header_admin.php'%}

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="?/admin/list">Administrator</a></li>
                    <li class="active">Verwaltung von Administratoren</li>
                </ol>
            </div>

            
			<div style="float: left">
    <form action= "?/admin/add" method="POST">
       Geben Sie neuen Namen ein: <input type="text" name="name" placeholder="Name" value="" />
        Legen Sie ein Passwort an:<input type="password" name="password" placeholder="Passwort" value="" />
        <input type="submit" name="save" value="Neuen Administrator hinzufuegen" />
    </form>
</div>
       <br>     
       <br>     
            <h4>Alle Administratoren</h4>

            <br/>

             <table class="table-bordered table-striped table">
                <tr>
                    <th>ID-Admin</th>
                    <th>Name</th>
                    <th>Passwort</th>
                    
                    <th>Bearbeiten</th>
                    <th>Loeschen</th>
                </tr>
                {%for item in adminList%}
                    <tr>
                        <td>{{item.id}}</td>
                        <td>{{item.name}}</td>
                        <td>{{item.password}}</td>
                       {%if item.name=='admin'%}
                        <td><p style="color:red"><i class="glyphicon glyphicon-minus-sign"></i></p></td>
						 <td><p style="color:red"><i class="glyphicon glyphicon-minus-sign"></i></p></td>
						{%else%}
                        <td><a href="?/admin/update/id/{{item.id}}" title="Bearbeiten"><i class="fa fa-pencil-square-o"></i></a></td>
						 <td><a href="?/admin/delete/id/{{item.id}}" title="Loeschen"><i class="fa fa-times"></i></a></td>
							{%endif%}
							
                    </tr>
					{%endfor%}
            </table>


        </div>
    </div>
</section>

{% include 'footer_admin.php'%}

