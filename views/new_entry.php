{% include 'header_admin.php'%}

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="?/category/list">Administrator</a></li>
                    <li class="active">Neue Fragen</li>
                </ol>
            </div>

          
       <br>     
       <br>
            
            <h4>Alle neuen Fragen</h4>

            <br/>
			{%if newEntries is empty%}
					<p style="color: red">{{'Keine neue Fragen'}}</p>
					{%else%}

            <table class="table-bordered table-striped table">
                <tr>
                    
                    <th>neue Fragen</th>
                    <th>Datum</th>
                    <th>Thema</th>
					<th>Status</th>
                    <th>Bearbeiten</th>
                    <th>Loeschen</th>
                </tr>
				
                {%for item in newEntries%}
					
						
                    <tr>
                       
                        <td><a href="?/request/edit/cat/{{item.category_id}}/id/{{item.id}}">{{item.text}}</a></td>
						 <td>{{item.dated}}</td>
						 <td>{{item.title}}</td>
							 {%if item.has_response is empty%}
                        <td><p style="color: red">{{'Antwort erwartet'}}</td>
							
							
                        <td><a href="?/request/edit/cat/{{item.category_id}}/id/{{item.id}}" title="Bearbeiten"><i class="fa fa-pencil-square-o"></i></a></td>
                        <td><a href="?/request/delete/cat/{{item.category_id}}/id/{{item.id}}" title="Loeschen"><i class="fa fa-times"></i></a></td>
                    </tr>
					{%endif%}
					{%endfor%}
					
            </table>
            {%endif%}
        </div>
    </div>
</section>

{% include 'footer_admin.php'%}

