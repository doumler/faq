{% include 'header_admin.php'%}

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="?/category/list">Administrator</a></li>
                    <li class="active">Verwaltung von Fragen</li>
                </ol>
            </div>
			<div style="float: right"><a href="?/request/new" class="btn btn-default back"><i class="fa fa-plus"></i>Alle neuen Fragen</a></div>

           <div style="float: left">
    <form action= "?/request/add/cat/{{entry.0.category_id}}" method="POST">
       Neue Frage: <input type="text" name="text" placeholder="Text" value="" />
        
        <input type="submit" name="save" value="Frage hinzufuegen" />
    </form>
</div>
       <br>     
       <br>
            
            <h4>Тема: {{entry.0.title}}</h4>

            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    
                    <th>Frage (Text)</th>
                    <th>Datum</th>
					 <th>Autor</th>
                    <th>Status</th>
                    <th>Verfuegbarkeit</th>
                    <th>Bearbeiten</th>
                    <th>Loeschen</th>
                </tr>
                {%for item in entry%}
					{%if item.text is empty%}
					<p style="color: red">{{'Zu diesem Thema gibt es noch keine Fragen'}}</p>
						{%else%}
                    <tr>
                       
                        <td>{{item.text}}</td>
						 <td>{{item.dated}}</td>
						 <td>{{item.name}}</td>
							 {%if item.has_response is empty%}
                        <td><p style="color: red">{{'Antwort erwartet'}}</td>
							{%else%}
						 <td>{{'Beantwortet'}}</td>
							 {%endif%}
							{%if item.is_published=='1'%}
							<td>{{'Veroeffentlicht'}}</td>
								{%else%}
                        <td>{{'Versteckt'}}</td>  
							{%endif%}
                        <td><a href="?/request/edit/cat/{{item.category_id}}/id/{{item.id}}" title="Bearbeiten"><i class="fa fa-pencil-square-o"></i></a></td>
                        <td><a href="?/request/delete/cat/{{item.category_id}}/id/{{item.id}}" title="Loeschen"><i class="fa fa-times"></i></a></td>
                    </tr>
					{%endif%}
					{%endfor%}
            </table>
            
        </div>
    </div>
</section>

{% include 'footer_admin.php'%}

