{% include 'header_admin.php'%}

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="?/admin/list">Administrator</a></li>
                    <li class="active">Verwaltung von Themen</li>
                </ol>
            </div>
			<div style="float: right"><a href="?/request/new" class="btn btn-default back"><i class="fa fa-plus"></i>Alle neuen Fragen</a></div>

           <div style="float: left">
    <form action= "?/category/add" method="POST">
       Neues Thema: <input type="text" name="name" placeholder="Name" value="" />
        
        <input type="submit" name="save" value="Thema hinzufuegen" />
    </form>
</div>
       <br>     
       <br>
            
            <h4>Alle Themen</h4>

            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID Thema</th>
                    <th>Titel</th>
                  
                    <th>Fragen</th>
                    <th>Veroeffentlicht</th>
                    <th>Antwort erwartet</th>
                    <th>Besichtigen</th>
					
                   
                    <th>Loeschen</th>
                </tr>
                {%for category in getAllCategories%}
                    <tr>
                        <td>{{category.category_id}}</td>
                        <td><a href="?/request/entry/cat/{{category.category_id}}">{{category.title}}</a></td>
                       
							{%if category.cat_id is empty%}
							<td>{{'0'}}</td>
								{%else%}
                        <td>{{category.requests}}</td>  
							{%endif%}
								{%if category.is_published is empty%}
								<td>{{'0'}}</td>
									{%else%}
                        <td>{{category.is_published}}</td>
							{%endif%}
                        <td>{{category.no_response}}</td>
						<td><a href="?/request/entry/cat/{{category.category_id}}" title="Besichtigen"><i class="fa fa-eye"></i></a></td>
                        <td><a href="?/category/delete/cat/{{category.category_id}}" title="Loeschen"><i class="fa fa-times"></i></a></td>
                    </tr>
					{%endfor%}
            </table>
            
        </div>
    </div>
</section>

{% include 'footer_admin.php'%}

