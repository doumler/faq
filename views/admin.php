{% include 'header_admin.php'%}

<section>
    <div class="container">
        <div class="row">
            
            <br/>
            
            <h4>Guten Tag, {{session.name}}!</h4>
            
            <br/>
            
            <p>Folgende Optionen sind fuer Sie verfuegbar:</p>
            
            <br/>
            
            <ul>
                <li><a href="?/admin/user">Verwaltung von Administratoren</a></li>
                <li><a href="?/category/list">Verwaltung von Themen, Fragen und Antworten</a></li>
                <li><a href="?/request/new">Besichtigung von neuen Fragen</a></li>
				
            </ul>
			
            
        </div>
    </div>
</section>

{% include 'footer_admin.php'%}

