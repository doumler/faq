{% include 'header_admin.php'%}

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="?/admin/list">Administrator</a></li>
                    <li class="active">Verwaltung von Administratoren</li>
                </ol>
            </div>

            
			
       <br>     
       <br>     
            <h4>Bearbeiten von Administratoren</h4>

            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID Admin</th>
                    <th>Name</th>
                    <th>Passwort</th>
                    
                    <th>Aktionen</th>
                   
                </tr>
				<form action="?/admin/update/id/{{adminToEdit.id}}" method="post">
               
                    <tr>
                        <td>{{adminToEdit.id}}</td>
                        <td>{{adminToEdit.name}}</td>
                        <td><input type="password" name="password" value="{{adminToEdit.password}}" /></td>
                       
                        <td><input type="submit" name="change" value="Aenderungen speichern" /></td>
                       
                    </tr>
					
					</form>
            </table>

        </div>
    </div>
</section>

{% include 'footer_admin.php'%}

