<?php
namespace models;
use lib\DataBase;

class Model{

protected $db = null;

	function __construct()
	{
		
		$this->db = DataBase::getDbConnection();
	}
	
/**
* get all themes
*/
	public function findAllСategories()
	{
		$sth = $this->db->prepare("SELECT * FROM category ORDER BY category_id");
		if ($sth->execute()) {
			return $sth->fetchAll();
		}
		return false;
	}
	/**
	* check if the session exists
	*/
	public function getCurrentUser()
    {
        if (isset($_SESSION['users'])) {
            return $_SESSION['users'];
        }

        header("Location: ?/admin/login");
    }
	/**
	* check if the answer exitsts 
	*/
	public function checkResponse($request_id){
	
	$stmt = $this->db->prepare("SELECT count(*) AS has_response FROM request INNER JOIN response ON response.request_id = request.id
	WHERE request.id = {$request_id}");
	$stmt->execute();
	$result =  $stmt->fetch();
	return $result['has_response'];
	
}
	
	
	

}

