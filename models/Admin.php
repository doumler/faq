<?php

namespace models;

class Admin extends Model{
	
	/**
	*	add admin
	*/
	public function addNewAdmin($name, $password){
	if(!empty($name) AND !empty($password))	
	{
	$stmt= $this->db->prepare("INSERT INTO admin (name, password) VALUES (:name, :password)");
	$stmt->execute(['name' =>trim($name), 'password'=>md5($password)]);
	}
	}
	
	/**
	*	list of admins
	*/
	public function selectAllAdmin(){
		
		$stmt = $this->db->prepare("SELECT * FROM admin");
		$stmt->execute();
		return $stmt->fetchAll();
		
	}
	
	/**
	*	selection of one admin to edit
	*/
	public function selectOneAdmin($id){
		
		$stmt = $this->db->prepare("SELECT * FROM admin WHERE id = :id");
		$stmt->execute(['id' =>$id]);
		return $stmt->fetch();
	}
	
	/**
	*	edition of admin
	*/
	public function updateAdminPass($id, $password){
		
		$stmt = $this->db->prepare("UPDATE admin SET password = :password WHERE id= :id");
		$stmt->execute(['password'=>md5($password), 'id'=>$id]);
		
	}
	
	/**
	*	deletion of admin
	*/
	public function deleteAdmin($admin){
		
	$stmt = $this->db->prepare("DELETE FROM admin WHERE id = :id");
	$stmt->execute(['id'=>$admin]);
		
	}
	
	/**
	*	authorization of admin
	*/
	public function findAuth($log, $pass)
	{
		$stmt = $this->db->prepare("SELECT * FROM admin WHERE name = :name AND password = :password");
		
		$stmt->execute(['name'=>$log, 'password'=>md5($pass)]);
		
		return $stmt->fetch(); 
		
		}
		
	
}











